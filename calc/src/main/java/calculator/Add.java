package calculator;

public class Add extends calculator {
    public Add() {}
    public Add(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
        double value = getLeftValue() + getRightValue();
        setResult(value);
    }
}