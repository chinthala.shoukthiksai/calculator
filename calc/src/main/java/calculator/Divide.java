package calculator;

public class Divide extends calculator {
    public Divide() {}

    public Divide(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
    	
        double value = getRightValue() != 0.0d ? getLeftValue() / getRightValue() : 0.0d;
        setResult(value);
    }
}