package calculator;

public class Multiply extends calculator {
    public Multiply() {}

    public Multiply(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
        double value = getLeftValue() * getRightValue();
        setResult(value);
    }
}
