package calculator;

public class Subtract extends calculator {
    public Subtract() {}

    public Subtract(double leftVal, double rightVal) {
        super(leftVal, rightVal);
    }

    @Override
    public void calculate() {
        double value = getLeftValue() - getRightValue();
        setResult(value);
    }
}