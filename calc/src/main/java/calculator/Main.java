package calculator;

public class Main {

    public static void main(String[] args) {
        

        calculator[] calculators = {
                new Divide(100.0d, 50.0d),
                new Add(25.0d, 92.0d),
                new Subtract(225.0d, 17.0d),
                new Multiply(11.0d, 3.0d)
        };

        for (calculator c: calculators) {
            c.calculate();
            System.out.print("result = ");
            System.out.print(c.getResult());
            System.out.println();
        }
    }
}
